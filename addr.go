package smux

import "fmt"

type smuxAddr struct {
	Addr string
}

func (*smuxAddr) Network() string {
	return "smux"
}

func (s *smuxAddr) String() string {
	return fmt.Sprintf("smux:%s", s.Addr)
}
