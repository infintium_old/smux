package smux

import (
	"encoding/binary"
	"fmt"
)

const (
	version = 2
)

const ( // cmds
	cmdSYN byte = iota // stream open
	cmdFIN             // stream close, a.k.a EOF mark
	cmdPSH             // data push
	cmdNOP             // no operation
)

const (
	sizeOfVer    = 1
	sizeOfCmd    = 1
	sizeOfLength = 2
	sizeOfSid    = 2
	sizeOfUid    = 2
	headerSize   = sizeOfVer + sizeOfCmd + sizeOfSid + sizeOfUid + sizeOfLength
)

// Frame defines a packet from or to be multiplexed into a single connection
type Frame struct {
	ver  byte
	cmd  byte
	sid  uint16
	uid  uint16
	data []byte
}

func newFrame(cmd byte, sid uint16, uid uint16) Frame {
	return Frame{ver: version, cmd: cmd, sid: sid, uid: uid}
}

type rawHeader []byte

func (h rawHeader) Version() byte {
	return h[0]
}

func (h rawHeader) Cmd() byte {
	return h[1]
}

func (h rawHeader) Length() uint16 {
	return binary.LittleEndian.Uint16(h[2:])
}

func (h rawHeader) StreamID() uint16 {
	return binary.LittleEndian.Uint16(h[4:])
}

func (h rawHeader) UserID() uint16 {
	return binary.LittleEndian.Uint16(h[6:])
}

func (h rawHeader) String() string {
	return fmt.Sprintf("Version:%d Cmd:%d StreamID:%d UserID:%d Length:%d",
		h.Version(), h.Cmd(), h.StreamID(), h.UserID(), h.Length())
}
